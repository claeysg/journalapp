var express = require('express');
var router = express.Router();
var app = express();
var path = require('path');
var bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({extended: true}));

var entry;


/* GET home page. */
router.get('/', function (req, res, next) {
  //res.render('index', { title: 'Express' });
  app.use(express.static(path.join(__dirname + '/public')));
  console.log(__dirname + '/textBox');
  res.sendFile(path.join(__dirname + '/textBox/textBox.html'));

});

router.post('/submitEntry', (req, res) => {
  console.log(req.body);
  entry = req.body;
});
module.exports = router;
